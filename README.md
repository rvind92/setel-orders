# Orders application - Setel Assignment

This microservice is responsible for retrieving, creating and updating orders made by the frontend client. The application also communicates with the Payment application through an HTTP endpoint to retrieve the 'payment' result generated from it and updates the database accordingly.

The solution is deployed on AWS Elastic Beanstalk in the NodeJS environment.

## Stack
- NodeJS + ExpressJS
- AWS Elastic Beanstalk
- Amazon RDS (MySQL)

## Running the application

1. Clone or download the repository, navigate into it and run the npm install command
```
git clone
cd setel-orders
npm install
```
2. To start the application, run the following command
```
npm start
```
3. To run the tests, run the following command
```
npm test
```
