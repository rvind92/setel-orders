const express = require('express')
const app = express()
const http = require('http').createServer(app)
const bodyParser = require('body-parser')
const cors = require('cors')
const { dequeue, getKeys } = require('./utils/queue')
const PORT = process.env.PORT || 3001
const SECONDS = 20 * 1000
const db = require('./setup/setup-db')

app.set('port', PORT)
app.use(bodyParser.json())
app.use(cors())

app.use('/api', require('./routes/orders'))

const updateStatus = () => {
	setInterval(async () => {
		try {
			const keys = await getKeys()
			if (keys.length === 0) return
			await db.order.update({status: 'DELIVERED'} , {
				where: {
					id: keys
				}
			})
			await dequeue(keys)
		} catch (e) {
			console.error({
				error: e, message: 'Update status error'
			})
		}
	}, SECONDS)
}

updateStatus()

db.sequelize.sync({force: true})
	.then(() => {
		const server = http.listen(app.get('port'), () => {
			console.log(`Orders app running at ${server.address().port}!`)
		})
	})
	.catch((err) => {
		console.error('Unable to connect to the database: ', err)
	})
