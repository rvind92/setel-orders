module.exports = (sequelize, DataTypes) => {
	return sequelize.define('order', {
    name: {
      type: DataTypes.STRING,
      allowNull: false
    },
		description: {
			type: DataTypes.STRING,
			allowNull: false,
			validate: {
				len: [1, 100]
			}
		},
		status: {
			type: DataTypes.STRING,
			allowNull: false,
			defaultValue: 'CREATED'
		}
	})
}
