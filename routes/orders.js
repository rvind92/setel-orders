const express = require('express')
const router = express.Router()
const axios = require('axios')
const db = require('../setup/setup-db')
const { enqueue, dequeue } = require('../utils/queue')
const BASE_URL = process.env.PAYMENT_URL || 'http://127.0.0.1:3002'

router.get('/', async (request, response) => {
	try {
		const ordersRetrieved = await db.order.findAll()
		return response.status(200).json(ordersRetrieved)
	} catch(e) {
		const message = 'Unable to retrieve orders'
		errorResponse(e, message, response)
	}
})

router.get('/:id', async (request, response) => {
	const { id } = request.params
	const condition = { where: { id } }

	try {
		const orderRetrieved = await db.order.findOne(condition)
		return response.status(200).json(ordersRetrieved)
	} catch(e) {
		const message = `Unable to retrieve order: ${id}`
		errorResponse(e, message, response)
	}
})

router.post('/', async (request, response) => {
	const { description, name } = request.body
	const values = { description, name }

	try {
		const orderCreated = await db.order.create(values)
		response.status(200).json(orderCreated)
		const makePayment = await axios.post(BASE_URL, { orderId: orderCreated.id })
		const { orderId, state } = makePayment.data
		const updateOrder = await db.order.update({ status: state }, {
			where: {
				id: orderId
			}
		})
		if(state !== 'CANCELLED') enqueue(orderId, state)
	} catch(e) {
		const message = 'Unable to create order'
		errorResponse(e, message, response)
	}
})

router.put('/', async (request, response) => {
	const { id } = request.body
	const value = { status: 'CANCELLED' }
	const condition = { where: { id } }

	try {
		await db.order.update(value, condition)
    const orderCancelled = await db.order.findOne(condition)
    dequeue(id)
		return response.status(200).json(orderCancelled)
	} catch(e) {
		const message = `Unable to cancel order: ${id}`
		errorResponse(e, message, response)
	}
})

const errorResponse = (error, errMsg, response) => {
	console.error({ error, errMsg })
	return response.status(400).json({
		error: errMsg
	})
}

module.exports = router
