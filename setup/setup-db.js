const Sequelize = require('sequelize')
const ENV = process.env.NODE_ENV || 'local'
const config = require('../config/config')[ENV]

const sequelize = new Sequelize(config.database, config.username, config.password, {
  host: config.host, dialect: 'mysql'
})

const db = {}

db.order = sequelize.import(__dirname + '/../models/order.js')
db.sequelize = sequelize
db.Sequelize = Sequelize

module.exports = db
