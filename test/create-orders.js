const mocha = require('mocha')
const faker = require('faker')
const chai = require('chai')
const { assert, expect } = chai

const db = require('../setup/setup-db')

describe('Creating order', () => {
  // Clear table
  before(async () => {
    const orders = []
    await db.order.destroy({ where: {}, truncate: true })
  })

  // Test will pass if we create an order
	it('should create one order', (done) => {
    const name = faker.name.findName()
    const description = faker.lorem.sentence()
    const order = { name, description }

    db.order.create(order).then(created => {
      expect(created).to.be.an('object')
      expect(created).to.have.property('id')
      expect(created).to.have.property('status')
      assert.isNotEmpty(created)
      assert.equal(created.name, name)
      assert.equal(created.description, description)
      assert.equal(created.status, 'CREATED')
      done()
    })
	})
})
