const mocha = require('mocha')
const faker = require('faker')
const chai = require('chai')
const { assert, expect } = chai

const db = require('../setup/setup-db')

describe('Retrieving order(s)', () => {
	// Clear table and create 10 dummy records
  before(async () => {
    const orders = []
    await db.order.destroy({ where: {}, truncate: true })
    for(let i = 0; i < 10; i++) {
      const name = faker.name.findName()
      const description = faker.lorem.sentence()
      const order = { name, description }
      orders.push(order)
    }
    await db.order.bulkCreate(orders)
  })

  // Test will pass if we get all orders
	it('should return all orders', (done) => {
      db.order.findAll().then(orders => {
        assert.isNotEmpty(orders)
        expect(orders).to.be.an('array')
        done()
      })
	})

  // Test will pass if we get one order
	it('should return one order', (done) => {
    db.order.findOne({
      order: [ [ 'createdAt', 'DESC' ] ]
    }).then(order => {
      assert.isNotEmpty(order)
      expect(order).to.be.an('object')
      done()
    })
	})
})
