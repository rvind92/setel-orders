const mocha = require('mocha')
const faker = require('faker')
const chai = require('chai')
const { assert, expect } = chai

const db = require('../setup/setup-db')

describe('Updating order(s)', () => {
  // Clear table and create 1 record each test in CONFIRMED state
  beforeEach(async () => {
    const orders = []
    await db.order.destroy({ where: {}, truncate: true })

    const name = faker.name.findName()
    const description = faker.lorem.sentence()
    const status = 'CONFIRMED'
    const order = { name, description, status }

    await db.order.create(order)
  })

  // Test will pass if we create an order
	it('should update order from CONFIRMED to DELIVERED state', (done) => {
    db.order
      .findOne({
        where: {
          status: 'CONFIRMED'
        }
      })
      .then(order => {
        return order.update({ status: 'DELIVERED' })
      }).then(updated => {
        expect(updated).to.be.an('object')
        expect(updated).to.have.property('id')
        expect(updated).to.have.property('status')
        assert.equal(updated.status, 'DELIVERED')
        done()
      })
	})

  it('should update order from CONFIRMED to CANCELLED state', (done) => {
    db.order
      .findOne({
        where: {
          status: 'CONFIRMED'
        }
      })
      .then(order => {
        return order.update({ status: 'CANCELLED' })
      }).then(updated => {
        expect(updated).to.be.an('object')
        expect(updated).to.have.property('id')
        expect(updated).to.have.property('status')
        assert.equal(updated.status, 'CANCELLED')
        done()
      })
	})
})
