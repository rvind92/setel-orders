const NodeCache = require('node-cache')
const ordersCache = new NodeCache()

const enqueue = async (id, state) => {
  await ordersCache.set(id, state)
}

const dequeue = async keys => {
  await ordersCache.del(keys)
}

const getKeys = async () => {
  const keys = await ordersCache.keys()
  return keys
}

module.exports = { enqueue, dequeue, getKeys }
